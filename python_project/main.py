#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Projects/{{ name | capitalize }}/main.py
#

# Common modules
from argparse import ArgumentParser, Namespace
import sys

# Utility modules
from tools.parser import CustomArgumentParser
from {{ name | lower }} import {{ name | capitalize }}

def main() -> int:
	"""
	"""
	parser: ArgumentParser = CustomArgumentParser(
		description='{{ description }}'
	)

	parser.add_argument(
		'',
		default='',
		help=''
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args(arguments)

	# Run script with arguments
	return {{ name | capitalize }}(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main(argv[1:]))
