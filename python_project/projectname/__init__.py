#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Projects/{{ name | capitalize }}/{{ name | lower }}/__init__.py
#

# Third-party modules
from dotenv import load_dotenv

# Utility modules
from tools.log import Log

# Import secrets
load_dotenv()

# Setting up logs
log = Log(__file__)


class {{ name | capitalize }}:
	"""
	"""

	def __init__(self) -> None:
		"""
		"""
		pass

	def __del__(self) -> None:
		"""
		"""
		pass

	def run(self) -> int:
		"""
		"""
		return 0
