/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * Memento Design Pattern
 *
 */

#pragma once

#include <stack>

namespace pattern {
	class Caretaker {
		private:
			std::stack<Memento> states;

		public:
			void push(Memento state) {
				states.push(state)
			}

			Memento pop() {
				return states.pop(state);
			}
	};

	class Memento {
		private:
			Content const content;

		public:
			Memento(Content content): content(content) {}

			Content getContent() {
				return content;
			}
	};

	class Originator {
		private:
			Content content;

		public:
			Memento createState() {
				return new Memento(content);
			}

			void restoreState(Memento state) {
				content = state.getContent();
			}

			Content getContent() {
				return content;
			}

			void setContent(Content content) {
				this.content = content;
			}
	};
}
