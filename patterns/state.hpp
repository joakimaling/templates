/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * State Design Pattern
 *
 */

#pragma once

namespace pattern {
	class Context() {
		private:
			State state;

		public:
			void request() {
				state.handle();
			}

			void setState(State state) {
				this.state = state;
			}
	};

	class State() {
		public:
			virtual void handle() = 0;
	};

	class ConcreteStateA(): public State {
		public:
			void handle() {}
	};

	class ConcreteStateB(): public State {
		public:
			void handle() {}
	};
}
