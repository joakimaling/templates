/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * Observer Design Pattern
 *
 */

#pragma once

#include <list>

class Observer {
	public:
		virtual ~Observer() = default;
		virtual void update() = 0;
};

class Subject {
	private:
		std::list<Observer*> observers;

	public:
		virtual ~Subject() = default;

		void attach(Observer& observer) {
			observers.push_back(&observer);
		}

		void detach(Observer& observer) {
			observers.remove(&observer);
		}

		void notify() {
			for (auto const& observer: observers) {
				observer->update();
			}
		}
};
