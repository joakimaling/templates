/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * Singleton Design Pattern
 *
 */

#pragma once

namespace pattern {
	template<class Type>
	class Singleton {
		protected:
			Singleton() = default;
			virtual ~Singleton() = default;

		public:
			// Scott Meyers mentions in his Effective Modern C++ book, that
			// deleted functions should generally be public as it results in
			// better error messages. This is due to compilers' behaviour to
			// check accessibility before deleted status
			Singleton(Singleton const&) = delete;
			Singleton(Singleton&&) = delete;
			Singleton& operator=(Singleton const&) = delete;
			Singleton& operator=(Singleton&&) = delete;

			static Type& getInstance() {
				static Type instance;
				return instance;
			}
	};
}
