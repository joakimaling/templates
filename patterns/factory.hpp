/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * Factory Design Pattern
 *
 */

#pragma once

#include <memory>
#include <string>

template<typename Type>
class Factory {
	private:

	public:
		Factory();
		~Factory();
		static std::unique_ptr<Type> create(std::string const&);
};
