/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * Builder Design Pattern
 *
 */

#pragma once

#include <memory>

template<typename Type>
class Builder {
	private:
		std::unique_ptr<Type> object;
	public:
		Builder() {
			object = new Type();
		}

		std::unique_ptr<Type> get() {
			return object;
		}
};

template<typename Type>
class Director {
public:
	virtual std::unique_ptr<Type> build(std::unique_ptr<Builder> builder);
};


