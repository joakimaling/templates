#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# ~/Scripts/{{ name | lower }}.py
#

# Common modules
from argparse import ArgumentParser, Namespace
from dataclasses import dataclass
import sys

# Third-party modules
from dotenv import load_dotenv

# Utility modules
from tools.log import Log
from tools.parse import CustomArgumentParser

# Import secrets
load_dotenv()

# Setting up logs
log = Log(__file__)


@dataclass
class {{ name | capitalize }}:
	"""
	"""

	def run(self) -> int:
		"""
		"""
		return 0


def main() -> int:
	parser: ArgumentParser = CustomArgumentParser(
		description='{{ description }}'
	)

	parser.add_argument(
		'-',
		'--',
		default='',
		help=''
	)

	# Run the parser and return the result
	data: Namespace = parser.parse_args(arguments)

	# Run script with arguments
	return {{ name | capitalize }}(**data.__dict__).run()


if __name__ == '__main__':
	sys.exit(main())
