# Templates

> A collection of templates

[![Licence][licence-badge]][licence-url]

A collection of code and configuration templates used in various projects. Some
files are used by class, project and ticket creation tools found in
[Scripts](https://gitlab.com/carolinealing/scripts).

## Installation

Simply clone it like this:

```sh
git clone git@gitlab.com:carolinealing/templates.git
```

## Usage

Copy any of the files into a project or use any of the tools found in
[Scripts](https://gitlab.com/carolinealing/scripts).

## Licence

Released under MIT. See [LICENSE.md][licence-url] for more.

Coded with :heart: by [Caroline Åling][user-url].

[licence-badge]: https://img.shields.io/gitlab/license/carolinealing%2Ftemplates
[licence-url]: LICENSE.md
[user-url]: https://gitlab.com/carolinealing
