# Merge Request template

Make sure you've read [CONTRIBUTING.md](../CONTRIBUTING.md) and followed all the
steps therein before creating this pull request.

## Title

*A short title summarising the bug fix or feature...*

## Summary

*A short summary, with as much relevant data as possible, of the problem and its
solution...*

## Issue reference

*A reference to the issue this pull request solves...*

Fixes issue [#100](https://gitlab.com/{user}/{slug}/-/issues/100)

## Proposed changes

*List your changes...*

- *...*
- *...*

@{{user}}
