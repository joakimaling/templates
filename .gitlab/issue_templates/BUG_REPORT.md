---
about: Report a bug you've found in this project
name: Bug Report
---

## Environment

- Hardware information: *... (if applicable)*
- Operating system (and version): *... (if applicable)*
- Web browser (and version): *... (if applicable)*
- Project version: *...*

## Observed behaviour

*Make a clear and concise description of the bug, and add screenshots to help
explaining your issue...*

![screenshot-01](screenshot1.png)

### Steps to reproduce

1. *...*
2. *...*

## Expected behaviour

*What's expected to happen...*
