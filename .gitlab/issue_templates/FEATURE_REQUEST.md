---
about: Suggest an idea or feature for this project
name: Feature Request
---

## Idea or problem

*Make a clear and concise summary of the idea you have or problem you face...*

## Suggested solution

*Describe your solution and how it fits with the project...*

## Considered alternatives

*Describe any other solutions you've considered...*
