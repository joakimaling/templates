# cd FirstLib/
# gcc -Wall -g -c first1.c -o first1.o
# gcc -Wall -g -c first2.c -o first2.o
#
# ar ruv libfirst.a first1.o first2.o
# ranlib libfirst.a
#
# Use: -L../FirstLib -libfirst

#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# Makefile
#

# Directories
build := ./build
projects := ~/Projects
release := ./release
resources := ./resources
source := ./source

# Compiler & linker flags
CFLAGS := -MMD -MP -Wall -Wextra -pedantic -std=c99
LDFLAGS := -L$(projects)/libraries -larray

# Include source code
INCLUDE := -I$(projects)/includes \
           -I$(source)

# Find all objects & dependencies
sources := $(shell find $(source) -name '*.c')
objects := $(sources:$(source)/%.c=$(build)/%.o)
depends := $(objects:%.o=%.d)

# Name of target
target := $(build)/{{ name | default('a.out') | lower }}

# Declare non-file targets
.PHONY: all build class clean debug release run tags

# Run all tasks
all: build $(target)

# Link objects into target
$(target): $(objects)
	$(CC) $(LDFLAGS) -o $@ $^

# Compile source code & create dependencies
$(build)/%.o: $(source)/%.c
	$(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $<

# Include dependencies
-include $(depends)

# Create & populate build directory
build:
	@mkdir -p $(dir $(objects))

# Create header & source files
class:
	(cd $(source) && create-class -L {{ licence }} -i -l C)

# Remove dependencies, objects & target
clean:
	-@rm -frv $(build)

# Compile debuggable code & run debugger
debug: CFLAGS += -DDEBUG -Og -g3
debug: clean all
	gdb $(target)

# Compile target optimised for release
release: CFLAGS += -DNDEBUG -O3
release: clean all
	@mkdir -p $(release)
	@cp $(resources)/* $(release)
	@cp $(target) $(release)

# Run target
run: all
	@./$(target)

# Create tag files
tags:
	ctags -R $(source)
